var Webpack = require('webpack');
var path = require('path');
var node_modules_dir = path.join(__dirname, 'node_modules');

var dependencies = [
    'react/dist/react.min.js',
    'moment/min/moment.min.js',
];

var config = {
    devtool: 'eval',
    entry: ['webpack/hot/dev-server', 'webpack-dev-server/client?http://localhost:3000', './static/js/app-react.jsx'],
    output: {
        path: path.resolve(__dirname, 'static', 'build'),
        publicPath: '/build/',
        filename: 'bundle.js'
    },
    module: {
        noParse: [],
        loaders: [
            {
                //tell webpack to use jsx-loader for all *.jsx files
                test: /\.jsx$/,
                loader: 'jsx-loader?insertPragma=React.DOM&harmony'
            },
            {
                test: path.resolve(node_modules_dir, dependencies[0]),
                loader: "expose?React"
            }
        ]
    },
    externals: {
        //don't bundle the 'react' npm package with our bundle.js
        //but get it from a global 'React' variable
        // 'react': 'React'
    },
    resolve: {
        extensions: ['', '.js', '.jsx'],
        alias: {
            
        }
    },
    plugins: [new Webpack.HotModuleReplacementPlugin()]
}

// Run through dependencies and extract the first part of the path, 
// as that is what you use to require the actual node modules 
// in your code. Then use the complete path to point to the correct
// file and make sure webpack does not try to parse it
dependencies.forEach(function(dependency) {
    var depPath = path.resolve(node_modules_dir, dependency);
    config.resolve.alias[dependency.split(path.sep)[0]] = depPath;
    config.module.noParse.push(depPath);
});

module.exports = config;