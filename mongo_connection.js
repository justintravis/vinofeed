var MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    connectionInstance;

module.exports = function(callback) {
    var url = 'mongodb://localhost:27017/vinofeed';

    if (connectionInstance) {
        if (callback) {
            callback(connectionInstance);
            return;
        } else {
            return connectionInstance;
        }
    }

    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        console.log("Connected correctly to server");

        connectionInstance = db;

        if (callback) {
            callback(connectionInstance);
        };
    });
};