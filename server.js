var fs = require('fs'),
    restify = require('restify'),
    httpProxy = require('http-proxy'),
    MongoConnection = require('./mongo_connection.js');

var controllers = {}, controllers_path = process.cwd() + '/controllers'
fs.readdirSync(controllers_path).forEach(function (file) {
    if (file.indexOf('.js') != -1) {
        controllers[file.split('.')[0]] = require(controllers_path + '/' + file)
    }
})

var server = restify.createServer();
var proxy = httpProxy.createProxyServer();

var isProduction = process.env.NODE_ENV === 'production';
 
server
    .use(restify.fullResponse())
    .use(restify.bodyParser())
    .use(restify.queryParser())

server.get("/api/1.0/articles", controllers.articles.getArticles);
server.get("/api/1.0/articlesById", controllers.articles.getArticlesById);
server.get("/api/1.0/article/click/:id", controllers.articles.articleClick);
// server.get("/api/1.0/sources", controllers.sources.getSources);
server.get("/feed", controllers.rss.getFeed);

var port = process.env.PORT || 3000;

// We only want to run the workflow when not in production
if (!isProduction) {

    // We require the bundler inside the if block because
    // it is only needed in a development environment. Later
    // you will see why this is a good idea
    var bundle = require('./bundle.js');
    bundle();

    // Any requests to localhost:3000/build is proxied
    // to webpack-dev-server
    server.get('/build/.*', function (req, res) {
        proxy.web(req, res, {
            target: 'http://localhost:8080'
        });
    });

    server.get('/sockjs-node/.*', function (req, res) {
        proxy.web(req, res, {
            target: 'http://localhost:8080'
        });
    });

    server.post('/sockjs-node/.*', function (req, res) {
        proxy.web(req, res, {
            target: 'http://localhost:8080'
        });
    });
}

server.get(/.*/, restify.serveStatic({
    'directory': 'static',
    'default': 'index.html'
}));

MongoConnection(function(db) {
    server.listen(port, function (err) {
        if (err)
            console.error(err)
        else
            console.log('App is ready at : ' + port)
    });
});

if (process.env.environment == 'production') {
    process.on('uncaughtException', function (err) {
        console.error(JSON.parse(JSON.stringify(err, ['stack', 'message', 'inner'], 2)))
    })
};
