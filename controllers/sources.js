var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');
 
exports.getSources = function(req, res) {
    var url = 'mongodb://localhost:27017/vinofeed';
    // Use connect method to connect to the Server
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        console.log("Connected correctly to server");

        var feedSources = db.collection('feedSources');

        feedSources.find({}).toArray(function(err, sources) {
            res.json(
                sources.map(function(source){
                    return {
                        name: source.name,
                        homepage: source.homepage
                    }
                })
            );
        });
    });
};