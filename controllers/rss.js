var ObjectID = require('mongodb').ObjectID,
    assert = require('assert'),
    sanitize = require('sanitize-html')
    RSS = require('rss'),
    MongoConnection = require('../mongo_connection.js');

var reduceString = function(string, maxLength) {
    /**
     * More intelligently reduces the long string. Looks to make a break at the sentence level, so the paragraph
     * remains more human readable.
     */
    var SEPARATOR = '. '
    var sentences = string.split(SEPARATOR);
    var reducedSentence = '';

    for (var i = 0; i < sentences.length; i++) {
        if (reducedSentence.length + sentences[i].length + SEPARATOR.length < maxLength) {
            reducedSentence += sentences[i] + SEPARATOR;
        };
    };

    return reducedSentence;
};

exports.getFeed = function(req, res) {
    var feed = new RSS({
        title: 'Vinofeed',
        description: 'All the latest wine news and reviews.'
    });
    var page = req.query.page || 1;
    var perPage = req.query.perPage || 20;

    MongoConnection(function(db){
        var feedItems = db.collection('feedItems');

        feedItems.find({}).sort({pubdate: -1}).skip(perPage * (page-1)).limit(perPage).toArray(function(err, items) {
            items.forEach(function(item){
                var sanitizedDescription = sanitize(item.description, { allowedTags: [] }).replace(/\n/g, ' ');
                feed.item({
                    title: item.title,
                    description: reduceString(sanitizedDescription, 300),
                    author: item.source,
                    url: 'http://vinofeed.co/api/1.0/article/click/' + item._id,
                    date: item.pubDate
                });
            });

            var xml = feed.xml();
            res.write(xml);
            res.end();
        });
    });
};