var ObjectID = require('mongodb').ObjectID,
    sanitize = require('sanitize-html'),
    MongoConnection = require('../mongo_connection.js');

var reduceString = function(string, maxLength) {
    /**
     * More intelligently reduces the long string. Looks to make a break at the sentence level, so the paragraph
     * remains more human readable.
     */
    var SEPARATOR = '. '
    var sentences = string.split(SEPARATOR);
    var reducedSentence = '';

    for (var i = 0; i < sentences.length; i++) {
        if (reducedSentence.length + sentences[i].length + SEPARATOR.length < maxLength) {
            reducedSentence += sentences[i] + SEPARATOR;
        };
    };

    return reducedSentence;
};
 
exports.getArticles = function(req, res) {
    var page = req.query.page || 1;
    var perPage = req.query.perPage || 20;

    MongoConnection(function(db){
        var feedItems = db.collection('feedItems');

        feedItems.find({}).sort({pubdate: -1}).skip(perPage * (page-1)).limit(perPage).toArray(function(err, items) {
            res.json({
                data: items.map(function(item){
                    var sanitizedDescription = sanitize(item.description, { allowedTags: [] }).replace(/\n/g, ' ');
                    return {
                        id: item._id,
                        title: item.title,
                        pubdate: item.pubdate,
                        description: reduceString(sanitizedDescription, 300),
                        source: item.source,
                        view_count: item.view_count,
                        link: item.link,
                        tracking_link: '/api/1.0/article/click/' + item._id
                    }
                })
            });
        });
    });
};

exports.getArticlesById = function(req, res) {
    var articleIds = req.query.ids.split(',');
    var objectIds = [];
    for (var i = 0; i < articleIds.length; i++) {
        objectIds.push(new ObjectID(articleIds[i]));
    };

    var page = req.query.page || 1;
    var perPage = req.query.perPage || 20;

    MongoConnection(function(db){
        var feedItems = db.collection('feedItems');

        feedItems.find({ _id: { $in: objectIds } }).sort({pubdate: -1}).skip(perPage * (page-1)).limit(perPage).toArray(function(err, items) {
            res.json({
                data: items.map(function(item){
                    var sanitizedDescription = sanitize(item.description, { allowedTags: [] }).replace(/\n/g, ' ');
                    return {
                        id: item._id,
                        title: item.title,
                        pubdate: item.pubdate,
                        description: reduceString(sanitizedDescription, 300),
                        source: item.source,
                        view_count: item.view_count,
                        link: item.link,
                        tracking_link: '/api/1.0/article/click/' + item._id
                    }
                })
            });
        });
    });
}

exports.articleClick = function(req, res) {
    var articleId = req.params.id;

    MongoConnection(function(db){
        var feedItems = db.collection('feedItems');

        feedItems.update({ _id: ObjectID(articleId) }, { $inc: { view_count: 1 } });

        feedItems.findOne({ _id: ObjectID(articleId) }, function(err, item){
            res.writeHead(302, {
              'Location': item.link
            });
            res.end();
        });
    })
}