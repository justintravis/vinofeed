var insertDocuments = function(db, callback) {
    // Get the documents collection
    var feedSources = db.collection('feedSources');
    // Clear the db first
    feedSources.remove({}, function(removeErr, removeResult){
        // Insert documents
        feedSources.insert([
            {
                name: 'Wine Spectator',
                url: 'http://www.winespectator.com/rss/rss?t=news',
                homepage: 'http://www.winespectator.com'
            },
            {
                name: 'Wine Folly',
                url: 'http://winefolly.com/feed/',
                homepage: 'http://winefolly.com'
            },
            {
                name: 'Wine Turtle',
                url: 'http://www.wineturtle.com/feed/',
                homepage: 'http://www.wineturtle.com'
            },
            {
                name: 'SourMashed',
                url: 'http://sourmashed.com/feed/',
                homepage: 'http://sourmashed.com'
            },
            {
                name: 'HoseMaster of Wine',
                url: 'http://feeds.feedburner.com/HosemasterOfWine',
                homepage: 'http://hosemasterofwine.blogspot.com/'
            },
            {
                name: 'Liz Palmer\'s Wine, Spirit & More Blog',
                url: 'http://www.liz-palmer.com/feed/',
                homepage: 'http://www.liz-palmer.com/'
            },
            {
                name: 'Great Northwest Wine',
                url: 'http://www.greatnorthwestwine.com/feed/',
                homepage: 'http://www.greatnorthwestwine.com/'
            },
            {
                name: 'Jameson Fink',
                url: 'http://jamesonfink.com/feed/',
                homepage: 'http://jamesonfink.com/',
            },
            {
                name: 'Fermentation Wine Blog',
                url: 'http://fermentationwineblog.com/feed/',
                homepage: 'http://fermentationwineblog.com/'
            },
            {
                name: 'Vinography',
                url: 'http://www.vinography.com/index.xml',
                homepage: 'http://www.vinography.com/'
            },
            {
                name: 'Decanter Wine News',
                url: 'http://www.decanter.com/feeds/news.rss',
                homepage: 'http://www.decanter.com/'
            }
        ], function(err, result) {
            assert.equal(err, null);
            console.log("Inserted initial feeds into the feedSources collection");
            callback(result);
        });
    });
}

var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');

// Connection URL
var url = 'mongodb://localhost:27017/vinofeed';
// Use connect method to connect to the Server
MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);
    console.log("Connected correctly to server");

    insertDocuments(db, function() {
        db.close();
    });
});