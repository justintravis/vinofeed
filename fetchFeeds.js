var FeedParser = require('feedparser'),
    request = require('request'),
    MongoConnection = require('./mongo_connection.js');

// Connection URL
var url = 'mongodb://localhost:27017/vinofeed',
    feedItems, feedSources;

var parseFeeds = function(feeds, db) {
    var numFeeds = feeds.length;
    var numProcessedFeeds = 0;
    for (var i = 0; i < numFeeds; i++) {
        (function(ii){
            console.log('parsing feed: ' + feeds[ii].name);
            var numFeedItems = 0;
            var req = request(feeds[ii].url);

            req.on('error', function (error) {
                // handle any request errors
                numProcessedFeeds++;
            });

            req.on('response', function (res) {
                var stream = this;

                if (res.statusCode != 200) return this.emit('error', new Error('Bad status code'));

                stream
                .pipe(new FeedParser())
                .on('readable', function() {
                    // This is where the action is!
                    var stream = this
                        , meta = this.meta // **NOTE** the "meta" is always available in the context of the feedparser instance
                        , item;

                    while (item = stream.read()) {
                        numFeedItems++;
                        stream.emit('feedItem', item);
                    }
                })
                .on('feedItem', function(pipedItem){
                    feedItems.find({link: pipedItem.link}).toArray(function(err, foundItem){
                        if (foundItem.length === 0) {
                            console.log('inserting ' + pipedItem.link + ' into db');
                            pipedItem.source = feeds[ii].name;
                            feedItems.insert(pipedItem);
                        };
                    });
                })
                .on('end', function(){
                    numProcessedFeeds++;
                    if (numProcessedFeeds === numFeeds) {
                        setTimeout(function(){
                            process.exit();
                        }, 1000);
                    };
                });
            });
        })(i);
    };
};

MongoConnection(function(db) {
    // Get the collections
    feedSources = db.collection('feedSources');
    feedItems = db.collection('feedItems');

    feedSources.find({}).toArray(function(err, feeds) {
        parseFeeds(feeds, db);
    });
});