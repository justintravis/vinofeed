/** @jsx React.DOM */

'use strict'

var React = require('react')
var ArticlesList = require('./components/ArticlesList.jsx')

React.render(<ArticlesList show="all" />, document.getElementById('articles-list'));

(function(){
    var _closeLightbox = function(e) {
        e.preventDefault();
        document.getElementById('overlay-curtain').classList.remove('visible');
        document.getElementById('email-signup-overlay').classList.remove('visible');
        document.getElementById('email-signup-overlay').querySelector('input[type=email]').value = '';
    };

    document.querySelector('.js-email-signup-toggle').addEventListener('click', function(e){
        e.preventDefault();
        document.getElementById('overlay-curtain').className += ' visible';
        document.getElementById('email-signup-overlay').className += ' visible';
        document.getElementById('email-signup-overlay').querySelector('input[type=email]').focus();
    });

    document.querySelector('.js-email-signup-close').addEventListener('click', _closeLightbox);
    document.getElementById('email-signup-overlay').querySelector('form').addEventListener('submit', function(e){
        setTimeout(_closeLightbox.bind(this, e), 500);
    });
    document.getElementById('overlay-curtain').addEventListener('click', _closeLightbox);
})();