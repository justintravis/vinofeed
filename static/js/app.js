require.config({
  baseUrl: "./js",

  paths: {
    "react": "react",
    "JSXTransformer": "JSXTransformer",
    "jquery": "jquery-1.11.3.min",
    "director": "director",
    "moment": "moment.min",
    "text": "text",
    "json": "json"
  },

  jsx: {
    fileExtension: '.jsx'
  }
});

require(['react', 'jquery', 'director', 'jsx!app-react'], function(React, moment, director, App) {
  App = React.createFactory(App);

  var router = Router({
        '/': function() {
            document.getElementById('articles-list').innerHTML = '';
            $('.show-bookmarks').removeClass('active');
            React.render(App({show: "all"}), document.getElementById('articles-list'));
        },
        '/bookmarks': function() {
            document.getElementById('articles-list').innerHTML = '';
            $('.show-bookmarks').addClass('active');
            React.render(App({show: "bookmarks"}), document.getElementById('articles-list'));
        },
    });

    router.init('/');

    $('header').find('a').on('click', function(e){
        e.preventDefault();
        router.setRoute(this.rel);
    });
});