/** @jsx React.DOM */

'use strict'

var currentBookmarks = JSON.parse(window.localStorage.getItem('bookmarks')) || [];

var React = require('react')
var Article = require('./Article.jsx');
var $ = require('jquery');

module.exports = React.createClass({
    getInitialState: function() {
        return {
            emptyBookmarks: false,
            articlesLoaded: false,
            articles: [],
            loadMoreText: 'Load more articles'
        }
    },
    componentWillMount: function() {
        this.props.page = 1;
    },
    componentDidMount: function() {
        this.loadArticles();
    },
    loadArticles: function(options) {
        if (this.props.show === 'bookmarks' && currentBookmarks.length === 0) {
            this.setState({
                emptyBookmarks: true
            });
            return;
        };

        this.setState({
            loadMoreText: 'Loading&hellip;'
        });

        var page = this.props.page;
        var url = (this.props.show === 'all') ? '/api/1.0/articles?page=' + page : '/api/1.0/articlesById?page=' + page + '&ids=' + currentBookmarks.join(',');
        var existingArticles = this.state.articles || [];
        $.get(url, function(articles) {
            var newArticles = articles.data;
            for (var i = 0; i < newArticles.length; i++) {
                existingArticles.push(newArticles[i]);
            };
            this.setState({
                articles: existingArticles,
                articlesLoaded: true,
                loadMoreText: 'Load more articles'
            });
        }.bind(this)).fail(function(){
            // call failed, reset page number
            this.props.page = page - 1;
            this.setState({
                loadMoreText: 'Load more articles'
            });
        }.bind(this));
    },
    loadMoreArticles: function(e) {
        e.preventDefault();
        var nextPage = this.props.page + 1;
        this.props.page = nextPage;
        this.props.loadMore = true;
        this.loadArticles();
    },
    render: function() {
        if (this.state.emptyBookmarks) {
            return (
                <div className={'inner-content no-bookmarks'}>
                    <h2>You don&rsquo;t have any bookmarks.</h2>
                    <p>You can add articles to your bookmarks by clicking the <i className={'icon-bookmark-o bookmark-icon'}></i> icon for each article.</p>
                </div>
            )
        } else {
            var articles = this.state.articles;
            var loadMore = this.props.loadMore;
            var page = this.props.page;

            return (
                <div>
                    {articles.map(function(article, index){
                        var prependAd = false;

                        if (index === 3 || (loadMore && index > 0 && index % 20 === 0)) {
                            prependAd = true;
                        }

                        return <Article article={article} prependAd={prependAd} />
                    })}
                    { this.state.articlesLoaded ? <a href="#" id="load-more" onClick={this.loadMoreArticles}><span dangerouslySetInnerHTML={{__html: this.state.loadMoreText}} /></a> : null }
                </div>
            )
        };
    }
});