/** @jsx React.DOM */

'use strict'

var React = require('react')
var AdBlock = require('./AdBlock.jsx');
var moment = require('moment');

var openWindow = function(url) {
    var newWindowSize = {
        width: 560,
        height: 320
    };

    var posX = (window.innerWidth / 2) - (newWindowSize.width / 2);
    var posY = (window.innerHeight / 2) - (newWindowSize.height / 2);

    window.open(url, undefined, 'width=' + newWindowSize.width + ', height=' + newWindowSize.height + ', left=' + posX + ', top=' + posY);
};

module.exports = React.createClass({
    getInitialState: function() {
        return {
            shareVisible: false
        }
    },
    componentWillMount: function() {
        this.props.article.pubdate = moment(this.props.article.pubdate).fromNow();
    },
    toggleArticleShare: function(e) {
        e.preventDefault();
        var shareVisible = this.state.shareVisible;
        this.setState({shareVisible: !shareVisible});
    },
    shareArticle: function(e) {
        e.preventDefault();
        var shareService = e.currentTarget.attributes.rel.value;
        var shareUrl = e.currentTarget.attributes['data-url'].value;
        openWindow(shareUrl);
        ga('send', 'event', 'share', 'click', shareService);
    },
    render: function() {
        var article = this.props.article;
        var prependAd = this.props.prependAd;

        return (
            <div>
                { prependAd ? <AdBlock format={'728x90'} /> : null }
                <div className={'article'}>
                    <p className={'title'}><a href={article.tracking_link} target="_blank" className={'article-link'}>{article.title}</a></p>
                    <p className={'description'}><a href={article.tracking_link} target="_blank" className={'article-link'}>{article.description}</a></p>
                    <div className={'meta'}>{article.source} &bull; {article.pubdate} &bull; {article.view_count || 0} views</div>
                    <p className={'meta actions'}><a href="#" className={'show-article-share'} onClick={this.toggleArticleShare}><i className={'icon-share'}></i>Share</a></p>
                    <div className={'article-share-sheet' + (this.state.shareVisible ? ' visible' : '')}>
                        <div className={'share-links-container'}>
                            <a href="#" className={'article-share-link'} rel="twitter" data-url={'https://twitter.com/intent/tweet?text=' + encodeURIComponent(article.title) + '&url=' + article.link + '&via=vinofeed'} onClick={this.shareArticle}><i className={'icon-twitter'}></i></a>
                            <a href="#" className={'article-share-link'} rel="facebook" data-url={'https://facebook.com/sharer.php?p[title]=' + encodeURIComponent(article.title) + '&p[url]=' + article.link} onClick={this.shareArticle}><i className={'icon-facebook'}></i></a>
                            <div><a href="#" className={'close-article-share'} onClick={this.toggleArticleShare}>&times;</a></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});