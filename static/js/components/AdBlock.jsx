/** @jsx React.DOM */

'use strict'

var React = require('react')
var ads = require("json!../ads.json");;

module.exports = React.createClass({
    getInitialState: function() {
        return {};
    },
    componentWillMount: function() {
        var formatKey = this.props.format;
        var adOptions = ads[formatKey];
        var adIndexMax = adOptions.length - 1;
        var chosenIndex = Math.floor(Math.random() * (adIndexMax + 1));
        var chosenAd = adOptions[chosenIndex];
        this.adMarkup = chosenAd;
    },
    getAdMarkup: function() {
        return this.adMarkup;
    },
    render: function() {
        return (
            <div className={'breaker-ad'}><p>Advertisement</p><div dangerouslySetInnerHTML={{__html: this.getAdMarkup()}}></div></div>
        )
    }
});