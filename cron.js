var cp = require('child_process');
var CronJob = require('cron').CronJob;

var job = new CronJob({
    cronTime: '*/5 * * * *',
    onTick: function() {
        console.log('Running cron at ' + new Date());
        cp.fork(__dirname + '/fetchFeeds.js');
    },
    start: false,
    timeZone: 'America/New_York'
});

job.start();